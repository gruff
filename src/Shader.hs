module Shader (shader) where

import Graphics.Rendering.OpenGL

shader :: Maybe FilePath -> Maybe FilePath -> IO Program
shader mV mF = do
  [p] <- genObjectNames 1
  vs <- case mV of
    Nothing -> return []
    Just v -> do
      [vert] <- genObjectNames 1
      source <- readFile v
      shaderSource vert $= [ source ]
      compileShader vert
--      print =<< get (shaderInfoLog vert)
      return [vert]
  fs <- case mF of
    Nothing -> return []
    Just f -> do
      [frag] <- genObjectNames 1
      source <- readFile f
      shaderSource frag $= [ source ]
      compileShader frag
--      print =<< get (shaderInfoLog frag)
      return [frag]
  attachedShaders p $= (vs, fs)
  linkProgram p
--  print =<< get (programInfoLog p)
  return p
