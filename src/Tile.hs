{-# LANGUAGE ForeignFunctionInterface #-}
module Tile where

import Prelude hiding (log)

import Control.Exception (bracketOnError)
import Control.Monad (forM_, when)
import Data.Bits (shiftL, shiftR, (.&.))
import Foreign (Ptr, castPtr, mallocArray, free, Word8, allocaBytes, withArray, peekArray, peekElemOff, pokeElemOff)
import Foreign.C (CInt(..), CUChar(..), CSize(..), withCStringLen)
import System.Directory (createDirectoryIfMissing)
import System.FilePath ((</>), (<.>))
import System.IO (withBinaryFile, IOMode(ReadMode,WriteMode), hPutBuf, hGetBuf)
--import Data.BEncode

import Fractal.GRUFF hiding (width, height, (</>))

import Compute (compute)
import QuadTree
import Utils (catchIO)

width, height, count, bytes :: Int
width  = 256
height = 256
count  = width * height
bytes  = count

rootSquare :: Square
rootSquare = Square{ squareSize = 8, squareWest = -4, squareNorth = -4 }

data Tile = Tile
  { tileQuad :: Quad
  , tileData :: Ptr CUChar
  }

mallocTile :: Quad -> IO Tile
mallocTile cs = do
  ds <- mallocArray count
  return $ Tile cs ds

freeTile :: Tile -> IO ()
freeTile (Tile _ ds) = do
  free ds

header :: String
header = "RuFfTtLe001\n"

writeTile :: FilePath -> Tile -> IO ()
writeTile cacheDir (Tile q ds) = do
  case filename q of
    Nothing -> return ()
    Just (dirs, file) -> do
      let dir = foldr1 (</>) (cacheDir : dirs)
      createDirectoryIfMissing True dir
      withBinaryFile (dir </> file <.> "t") WriteMode $ \h -> do
        withCStringLen header $ \(p, l) -> hPutBuf h p l
        withArray (wordBE bytes) $ \p -> hPutBuf h p 4
        hPutBuf h ds bytes
--        hPut h . bPack . metaData $ q
  where
    wordBE :: Int -> [Word8]
    wordBE n = map (\b -> fromIntegral $ (n `shiftR` (8 * b)) .&. 0xFF) [3, 2, 1, 0]

{-
metaData :: Quad -> BEncode
metaData q@Quad{ quadLevel = l, quadWest = w, quadNorth = n } =
  BDict $ fromList [ ("tile", BDict $ fromList
    [ ("about", BDict $ fromList
      [ ("version", BInt 1)
      , ("generator", BString $ pack "gruff-0.1") ]
    , ("images", BDict . fromList . map image . zip [0..] $
      [ "continuous dwell", "normalized distance", "final angle"]) ]
    ]
  where
    image plane alg = (alg, BDict $ fromList
      [ ("width", BInt (fromIntegral width))
      , ("height", BInt (fromIntegral height))
      , ("real", BInt w)
      , ("imag", BInt (negate n))
      , ("scale", BInt (fromIntegral l + 2))
      , ("format", BString $ pack "float32le")
      , ("order", BString $ pack "lr,tb")
      , ("data offset", BInt (fromIntegral $ plane * count * sizeOf (0 :: CFloat)))
      ])
-}

readTile :: FilePath -> Quad -> IO (Maybe Tile)
readTile cacheDir q = flip catchIO (\_ -> return Nothing) $ do
  case filename q of
    Nothing -> return Nothing
    Just (dirs, file) -> do
      let dir = foldr1 (</>) (cacheDir : dirs)
      bracketOnError (mallocTile q) freeTile $ \t@(Tile _ ds) -> do
        withBinaryFile (dir </> file <.> "t") ReadMode $ \h -> do
          let headerBytes = 12
          allocaBytes headerBytes $ \p -> do
            headerBytes' <- hGetBuf h p headerBytes
            when (headerBytes /= headerBytes') $ fail "readTile header fail"
            header' <- peekArray headerBytes p
            when (header' /= (map (fromIntegral . fromEnum) header :: [Word8])) $ fail "readTile header mismatch"
          dataBytes <- allocaBytes 4 $ \p -> do
            lenBytes' <- hGetBuf h p 4
            when (lenBytes' /= 4) $ fail "readTile header length fail"
            unwordBE `fmap` peekArray 4 p
          when (dataBytes /= bytes) $ fail "readTile header length mismatch"
          bytes' <- hGetBuf h ds bytes
          when (bytes /= bytes') $ fail ("readTile " ++ show bytes ++ " /= " ++ show bytes')
          return $ Just t
  where
    unwordBE :: [Word8] -> Int
    unwordBE = sum . zipWith (\b n -> fromIntegral n `shiftL` (8 * b)) [3, 2, 1, 0]

clearTile :: Tile -> IO ()
clearTile (Tile _ ds) = clear ds >> return ()
  where
    clear p = c_memset (castPtr p) 0 (fromIntegral bytes)

computeTile :: (String -> IO ()) -> Tile -> IO Bool
computeTile log (Tile q@Quad{ quadLevel = l } ds) = do
    its <- compute'
    log $ show ("getTile", q, "computed", its)
    return $ its /= 0
  where
    compute' = reifyPrecision (fromIntegral l + 8) $ \prec ->
      let _ = fmap precisionOf c `asTypeOf` (prec :+ prec)
          c = cx :+ cy
          cx = fromRational (squareWest s)
          cy = fromRational (squareNorth s)
      in  compute ds c l m'
    m' = 1000000
    s = square rootSquare q

data TileResult = Interrupted | Completed !Tile | BlockedOn !Quad

getTile :: (String -> IO ()) -> FilePath -> Quad -> IO TileResult
getTile log cacheDir q = do
  mTile <- readTile cacheDir q
  case mTile of
    Just t -> do
      log $ show ("getTile", q, "read")
      return (Completed t)
    Nothing -> case parent q of
      Just (ch, q') -> do
        mptile <- readTile cacheDir q'
        case mptile of
          Nothing -> do
            log $ show ("getTile", q, "blocked")
            return (BlockedOn q')
          Just t' -> do
            log $ show ("getTile", q, "compute")
            t <- mallocTile q
            inheritTile ch t' t
            freeTile t'
            touchTileBorder t
            ok <- computeTile log t
            if ok
              then writeTile cacheDir t >> return (Completed t)
              else freeTile t >> return Interrupted
      Nothing -> do
        log $ show ("getTile", q, "compute")
        t <- mallocTile q
        clearTile t
        touchTileBorder t
        ok <- computeTile log t
        if ok
          then writeTile cacheDir t >> return (Completed t)
          else freeTile t >> return Interrupted

inheritTile :: Child -> Tile -> Tile -> IO ()
inheritTile c (Tile _ from) (Tile _ to) = do
  let coff = case c of
                NorthWest -> 0
                NorthEast -> 128
                SouthWest -> 256 * 128
                SouthEast -> 256 * 128 + 128
  forM_ [0..255] $ \j -> do
    forM_ [0..255] $ \i -> do
      let toff = 256 * j + i
          foff = 256 * (j `div` 2) + (i `div` 2) + coff
      pokeElemOff to toff =<< peekElemOff from foff

touchTileBorder :: Tile -> IO ()
touchTileBorder (Tile _ t) = do
  let o i j = 256 * j + i
  forM_ [0..255] $ \e -> do
    pokeElemOff t (o   0   e) 254
    pokeElemOff t (o   e   0) 254
    pokeElemOff t (o 255   e) 254
    pokeElemOff t (o   e 255) 254

foreign import ccall unsafe "string.h memset"
  c_memset :: Ptr Word8 -> CInt -> CSize -> IO (Ptr Word8)
