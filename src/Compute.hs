{-# LANGUAGE FlexibleContexts, MultiParamTypeClasses #-}
module Compute (compute) where

import Control.Monad (forM_, forM, liftM2, when)
--import Control.Parallel.Strategies (parMap, rseq)
import Data.Bits (bit)
import Data.Either (partitionEithers)
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import Data.Ix (range, inRange)
import Data.Maybe (catMaybes)
import Data.Monoid (mappend)
import Data.Set (Set)
import qualified Data.Set as S
import Foreign (Ptr, peekElemOff, pokeElemOff)
import Foreign.C (CUChar)

--import Numeric.QD (DoubleDouble(DoubleDouble), QuadDouble(QuadDouble))
import Data.Array.BitArray.IO hiding (map)

import Fractal.GRUFF

data Iter z d = Iter
  { iterZ :: !(Complex z)
  , iterD :: !(Complex d)
  , iterN :: !Int
  , iterI :: !Int
  , iterJ :: !Int
  }

instance Eq (Iter z d) where
  a == b = iterI a == iterI b && iterJ a == iterJ b
  a /= b = iterI a /= iterI b || iterJ a /= iterJ b

instance Ord (Iter z d) where
  a `compare` b = (iterI a `compare` iterI b) `mappend` (iterJ a `compare` iterJ b)

initialIter :: (Num z, Num (Complex z), Num d, Num (Complex d)) => Int -> Int -> Iter z d
initialIter i j = Iter{ iterZ = 0, iterD = 0, iterN = 0, iterI = i, iterJ = j }

tileBounds :: ((Int, Int), (Int, Int))
tileBounds = ((0, 0), (255, 255))

compute :: NaturalNumber p => Ptr CUChar -> Complex (VFixed p) -> Int -> Int -> IO Int
compute io_de in_c@(cx0:+_) in_level in_iters = do
  maxIters <- newIORef (-1)
  active <- newArray tileBounds False
  finished <- newArray tileBounds False
  let prec = precision cx0
      loop'
        ::(NaturalNumber p,
           Floating d,
           Fractional z,
           Num (Complex z),
           Num (Complex d),
           Real z,
           Real d,
           ComplexRect (Complex z) z,
           ComplexRect (Complex d) d,
           RealToFrac' (VFixed p) z,
           RealToFrac' z d,
           RealToFrac' d Double)
        => Complex (VFixed p) -> Set (Iter z d) -> IO Int
      loop' c = loop c active finished maxIters False 1024
      init'
        :: (Num z, Num (Complex z), Num d, Num (Complex d))
        => z -> d -> IO (Set (Iter z d))
      init' = initialize active finished
  case prec of
    _ | prec < 48  -> loop' in_c =<< init' double double
--      | prec < 96  -> loop' in_c =<< init' ddouble double
--      | prec < 192 -> loop' in_c =<< init' qdouble double
      | prec < 480 -> loop' in_c =<< init' cx0 double
      | otherwise  -> loop' in_c =<< init' cx0 f24

  where

    double :: Double
    double = 0
{-
    ddouble :: DoubleDouble
    ddouble = 0
    qdouble :: QuadDouble
    qdouble = 0
-}
    initialize
      :: (Num z, Num (Complex z), Num d, Num (Complex d))
      => IOBitArray (Int, Int) -> IOBitArray (Int, Int) -> z -> d -> IO (Set (Iter z d))
    initialize active finished _ _ = (S.fromList . map (uncurry initialIter) . catMaybes) `fmap` mapM (initialize1 active finished) (range tileBounds)

    initialize1 :: IOBitArray (Int, Int) -> IOBitArray (Int, Int) -> (Int, Int) -> IO (Maybe (Int, Int))
    initialize1 active finished ij = do
      me <- peekElemOff io_de (off ij)
      case me of
        _ | me == 255 -> writeArray finished ij True >> return Nothing
          | me >  0   -> writeArray active   ij True >> return (Just ij)
          | otherwise ->                                return Nothing
{-
            do
              edge <- fmap or $ forM (range ((i-1,j-1),(i+1,j+1))) $ \iijj -> do
                case inRange tileBounds iijj of
                  False -> return False
                  True  -> (0 <) `fmap` peekElemOff io_de (off iijj)
              case edge of
                False -> return Nothing
                True  -> return (Just ij)
-}

    off :: (Int, Int) -> Int
    off (i, j) = 256 * j + i

    loop
      :: (NaturalNumber p, RealToFrac' (VFixed p) z, RealToFrac' z d, RealToFrac' d Double, Real z, Real d, Fractional z, Fractional d, Floating d, ComplexRect (Complex z) z, ComplexRect (Complex d) d, Num (Complex z), Num (Complex d))
      => Complex (VFixed p) -> IOBitArray (Int, Int) -> IOBitArray (Int, Int) -> IORef Int -> Bool -> Int -> Set (Iter z d) -> IO Int
    loop c0 active finished maxIters progressed step_iters activeSet
      | S.null activeSet = done maxIters activeSet
      | step_iters >= in_iters = done maxIters activeSet
      | otherwise = loop' False S.empty activeSet
      where
        loop' escaped unescaped todoss
          | S.null todoss && not escaped && progressed = done maxIters unescaped
          | S.null todoss && not escaped = loop c0 active finished maxIters progressed (2 * step_iters) unescaped
          | S.null todoss &&     escaped = loop c0 active finished maxIters True (2 * step_iters) unescaped
          | otherwise = do
              let (escapees, unescapees) = partitionEithers . map (\todo -> calculate step_iters (coord c0 todo) todo) . S.toList $ todoss
              nexts <- (S.fromList . concat) `fmap` mapM (finalize active finished maxIters) escapees
              let olds = S.fromList unescapees `S.union` unescaped
                  news = nexts `S.difference` olds -- left biased
                  escaped' = escaped || not (S.null news)
              loop' escaped' olds news

    escapeRadius2 :: Num z => z
    escapeRadius2 = 65536

    scaleI :: Integer
    scaleI = 32 * bit in_level

    scale' :: Fractional z => z
    scale' = recip (fromInteger scaleI)

    scale :: Num d => d
    scale = fromInteger scaleI

    coord :: (NaturalNumber p, RealToFrac' (VFixed p) z) => Complex (VFixed p) -> Iter z d -> Complex z
    coord c0 iter = fmap realToFrac' $
      c0 + scale' .* (fromIntegral (iterI iter) :+ fromIntegral (iterJ iter))

    calculate :: (RealToFrac' z d, Real z, Real d, Fractional z, Fractional d, ComplexRect (Complex z) z, Num (Complex z), Num (Complex d)) => Int -> Complex z -> Iter z d -> Either (Iter z d) (Iter z d)
    calculate step_iters c iter
      | magnitudeSquared (iterZ iter) >= escapeRadius2 = Left iter
      | step_iters <= 0 = Right iter
      | otherwise = calculate (step_iters - 1) c (step c iter)

    step :: (RealToFrac' z d, Real z, Real d, Fractional z, Fractional d, ComplexRect (Complex z) z, Num (Complex z), Num (Complex d)) => Complex z -> Iter z d -> Iter z d
    step c i@Iter{ iterZ = z, iterD = d, iterN = n } =
      i{ iterZ = sqr z + c, iterD = let zd = fmap realToFrac' z * d in zd + zd + 1, iterN = n + 1 }

    finalize
      :: (RealToFrac' z d, RealToFrac' d Double, Real z, Real d, Fractional z, Fractional d, Floating d, ComplexRect (Complex d) d, Num (Complex z), Num (Complex d))
      => IOBitArray (Int, Int) -> IOBitArray (Int, Int) -> IORef Int -> Iter z d -> IO [Iter z d]
    finalize active finished maxIters Iter{ iterZ = z, iterD = d, iterN = n, iterI = i, iterJ = j } = do
      m <- readIORef maxIters
      when (n > m) $ writeIORef maxIters n
      let k = off (i, j)
          z' = fmap realToFrac' z `asTypeOf` d
          z2 = magnitudeSquared z'
          diste :: Double
          diste = realToFrac' $ log z2 * sqrt (z2 / magnitudeSquared d) * scale
          disteB
            | diste > 4 = 255
            | otherwise = round . clamp 1 254 . (* 255) . (/ 4) $ diste
      pokeElemOff io_de k disteB
      writeArray finished (i, j) True
      writeArray active (i, j) False
      fmap catMaybes $ forM (liftM2 (,) [i-1,i,i+1] [j-1,j,j+1]) $ \iijj@(ii, jj) -> do
        if not (inRange tileBounds iijj) then return Nothing else do
          fin <- readArray finished iijj
          act <- readArray active iijj
          if fin || act then return Nothing else do
            writeArray active iijj True
            return $ Just (initialIter ii jj)

    done :: IORef Int -> Set (Iter z d) -> IO Int
    done maxIters activeSet = do
      forM_ (S.toList activeSet) $ \Iter{ iterI = i, iterJ = j } -> do
        pokeElemOff io_de (off (i, j)) 0
      readIORef maxIters

clamp :: Ord a => a -> a -> a -> a
clamp mi ma x = mi `max` x `min` ma

class RealToFrac' a b where
  realToFrac' :: a -> b

instance RealToFrac' Double Double where
  realToFrac' = id

instance NaturalNumber p => RealToFrac' (VFixed p) (VFixed p) where
  realToFrac' = id

instance NaturalNumber p => RealToFrac' (VFloat p) (VFloat p) where
  realToFrac' = id

instance NaturalNumber p => RealToFrac' (VFixed p) Double where
  realToFrac' = realToFrac

instance (NaturalNumber p, NaturalNumber q) => RealToFrac' (VFixed p) (VFloat q) where
  realToFrac' = realToFrac -- FIXME better way?

instance NaturalNumber p => RealToFrac' (VFloat p) Double where
  realToFrac' = uncurry encodeFloat . decodeFloat
{-
instance RealToFrac' DoubleDouble Double where
  realToFrac' (DoubleDouble x _) = realToFrac x

instance RealToFrac' QuadDouble Double where
  realToFrac' (QuadDouble x _ _ _) = realToFrac x

instance NaturalNumber p => RealToFrac' (VFixed p) DoubleDouble where
  realToFrac' = realToFrac

instance NaturalNumber p => RealToFrac' (VFixed p) QuadDouble where
  realToFrac' = realToFrac
-}
