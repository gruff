module TileShake (computeTiles) where

import Development.Shake
import Development.Shake.FilePath

import GHC.Conc (numCapabilities)
import Control.Concurrent (forkIO, killThread, newEmptyMVar, putMVar, takeMVar)
import Control.Exception (finally)
import Control.Monad (forM_)
import Data.List (isPrefixOf)
import Data.Set (Set)
import qualified Data.Set as S
import System.Directory (createDirectoryIfMissing)

import Tile
import QuadTree

toFilename :: FilePath -> Quad -> FilePath
toFilename cacheDir q = case filename q of
  Just (dirs, file) -> foldr1 (</>) (cacheDir : dirs) </> file <.> "t"
  Nothing -> error $ "gruff.TileShake.toFilename: " ++ show (cacheDir, q)

toQuad :: FilePath -> FilePath -> Maybe Quad
toQuad cacheDir f
  | takeExtension f == ".t" && cacheDir' `isPrefixOf` f = fromFilename f'
  | otherwise = Nothing
  where
    cacheDir' = addTrailingPathSeparator cacheDir
    f' = drop (length cacheDir') . dropExtension $ f

computeTiles :: (String -> IO ()) -> (Tile -> IO ()) -> FilePath -> Set Quad -> IO (IO ())
computeTiles output done cacheDir qs = do
  let shakeDir = cacheDir </> "_shake"
  createDirectoryIfMissing True shakeDir
  m <- newEmptyMVar
  t <- forkIO . flip finally (putMVar m ()) $ do
    shake shakeOptions
        { shakeFiles = shakeDir
        , shakeThreads = numCapabilities
        , shakeReport = Nothing -- Just (cacheDir </> "_shake-report.html")
        , shakeVerbosity = Quiet
        } $ do
      want (map (toFilename cacheDir) (S.toList qs))
      "//*.t" *> \out -> do
        let Just q = toQuad cacheDir out
        mme <- liftIO $ readTile cacheDir q
        case mme of
          Just t -> liftIO $ if q `S.member` qs then done t else freeTile t
          Nothing -> case parent q of
            Nothing -> traced ("compute " ++ show q) $ do
              t <- mallocTile q
              clearTile t
              touchTileBorder t
              ok <- computeTile output t
              if ok
                then writeTile cacheDir t >> if q `S.member` qs then done t else freeTile t
                else freeTile t >> fail "intr"
            Just (ch, q') -> (need [toFilename cacheDir q'] >>) . traced ("compute " ++ show q) $ do
              mptile <- readTile cacheDir q'
              case mptile of
                Nothing -> fail "eeps"
                Just t' -> do
                  t <- mallocTile q
                  inheritTile ch t' t
                  freeTile t'
                  touchTileBorder t
                  ok <- computeTile output t
                  if ok
                    then writeTile cacheDir t >> if q `S.member` qs then done t else freeTile t
                    else freeTile t >> fail "intr"
    forM_ (S.toList qs) $ \q -> do
      mt <- readTile cacheDir q
      case mt of
        Nothing -> return ()
        Just t -> done t
  return (killThread t >> takeMVar m)
