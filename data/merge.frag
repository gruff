uniform sampler2D sheet0;
uniform sampler2D sheet1;
uniform float blend;

void main() {
  vec2 p = gl_TexCoord[0].xy;
  gl_FragColor = mix(texture2D(sheet1, p), texture2D(sheet0, p), blend);
}
