uniform sampler2D de;
uniform sampler2D it;
uniform sampler2D tt;

uniform float hshift;
uniform float hscale;

float lin_interp(float x, float domain_low, float domain_hi, float range_low, float range_hi) {
  if ((x >= domain_low) && (x <= domain_hi)) {
    x = (x - domain_low) / (domain_hi - domain_low);
    x = range_low + x * (range_hi - range_low);
  }
  return x;
}

float pvp_adjust_3(float x) {
  // red
  x = lin_interp(x, 0.00, 0.125, -0.050, 0.090);
  // orange
  x = lin_interp(x, 0.125, 0.25,  0.090, 0.167);
  // yellow
  x = lin_interp(x, 0.25, 0.375,  0.167, 0.253);
  // chartreuse
  x = lin_interp(x, 0.375, 0.50,  0.253, 0.383);
  // green
  x = lin_interp(x, 0.50, 0.625,  0.383, 0.500);
  // teal
  x = lin_interp(x, 0.625, 0.75,  0.500, 0.667);
  // blue
  x = lin_interp(x, 0.75, 0.875,  0.667, 0.800);
  // purple
  x = lin_interp(x, 0.875, 1.00,  0.800, 0.950);
  return(x);
}

vec3 hsv2rgb(float h, float s, float v)
{
  float i, f, p, q, t, r, g, b;
  if (s == 0.0) {
    // Ignore hue
    r = v;
    g = v;
    b = v;
  } else {
    /* Apply physiological mapping: red, yellow, green and blue should
       be equidistant. */
    h = pvp_adjust_3(h);
    h = h - floor(h);
    h = h * 6.0;
    i = floor(h);
    f = h - i;
    p = v*(1.0 - s);  // The "low-flat" curve
    q = v*(1.0 - (s*f));  // The "falling" curve
    t = v*(1.0 - (s*(1.0 - f)));  // The "rising" curve
    if (i < 1.0) { r = v; g = t; b = p; } else
    if (i < 2.0) { r = q; g = v; b = p; } else
    if (i < 3.0) { r = p; g = v; b = t; } else
    if (i < 4.0) { r = p; g = q; b = v; } else
    if (i < 5.0) { r = t; g = p; b = v; } else
                 { r = v; g = p; b = q; }
  }
  return vec3(r, g, b);
}

void main(void) {
  float d = texture2D(de, gl_TexCoord[0].st).x;
  float i = texture2D(it, gl_TexCoord[0].st).x;
  float t = texture2D(tt, gl_TexCoord[0].st).x;
  vec4 c = vec4(1.0, 0.0, 0.0, 1.0);
  if (i > 0.0) {
/*
    float h = hscale * log(i) + hshift;
    h -= floor(h);
    float s;
    float v;
    if (t            > 0.0) { s = 0.25; } else { s = 0.15; }
    i /= 2.0;
    if (i - floor(i) < 0.5) { v = 0.95; } else { v = 1.00; }
    c.rgb = hsv2rgb(h, s, v);
    c.rgb *= clamp(0.5 + 0.25 * log(d), 0.0, 1.0);
*/
    c.rgb = vec3(clamp(0.5 + 0.25 * log(d), 0.0, 1.0));
  }
  gl_FragColor = c;
}
