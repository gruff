uniform sampler2D de;

uniform vec3 interior;
uniform vec3 border;
uniform vec3 exterior;

void main(void) {
  float d = texture2D(de, gl_TexCoord[0].st).x;
  vec4 c = vec4(interior, 1.0);
  if (d > 0.0) {
    float k = pow(clamp(0.5 + 0.25 * log2(4.0 * d), 0.0, 1.0), 2.0);
    c.rgb = mix(border, exterior, k);
  }
  gl_FragColor = c;
}
