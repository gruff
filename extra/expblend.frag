uniform sampler2D outer;
uniform sampler2D inner;

void main(void) {
  vec2 p = gl_TexCoord[0].st;
  vec2 q = 0.5 * pow(0.5, p.y) * vec2(cos(p.x), sin(p.x));
  vec4 o = texture2D(outer, vec2(0.5) + 0.5 * q);
  vec4 i = texture2D(inner, vec2(0.5) +       q);
  gl_FragColor = mix(o, i, p.y);
}
