#ifndef MP_REAL_H
#define MP_REAL_H 1

#include <mpfr.h>
#define MPFR_RNDN GMP_RNDN

class mp_real {
public:
  mpfr_t m;
  mp_real() {
    mpfr_init2(m, 53);
    mpfr_set_d(m, 0, MPFR_RNDN);
  }
  mp_real(const double &s) {
    mpfr_init2(m, 53);
    mpfr_set_d(m, s, MPFR_RNDN);
  }
  mp_real(const int32_t &s) {
    mpfr_init2(m, 53);
    mpfr_set_si(m, s, MPFR_RNDN);
  }
  mp_real(const mp_real &s) {
    mpfr_init2(m, mpfr_get_prec(s.m));
    mpfr_set(m, s.m, MPFR_RNDN);
  }
  mp_real(mpfr_prec_t p) {
    mpfr_init2(m, p);
  }
  mp_real(mpfr_t s, mpfr_prec_t p) {
    mpfr_init2(m, p);
    mpfr_set(m, s, MPFR_RNDN);
  }
  mp_real(const char *s, mpfr_prec_t p) {
    mpfr_init2(m, p);
    mpfr_set_str(m, s, 10, MPFR_RNDN);
  };
  mp_real(double s, mpfr_prec_t p) {
    mpfr_init2(m, p);
    mpfr_set_d(m, s, MPFR_RNDN);
  };
  ~mp_real() {
    mpfr_clear(m);
  }
#define maxprec(x, y) max(mpfr_get_prec(x.m), mpfr_get_prec(y.m))
  inline mp_real& operator=(mp_real const &y) {
    mpfr_set_prec(m, maxprec((*this), y));
    mpfr_set(m, y.m, MPFR_RNDN);
    return *this;
  }
};

inline bool operator<(mp_real const &x, mp_real const &y) {
  return mpfr_less_p(x.m, y.m);
}

inline mp_real operator+(mp_real const &x, mp_real const &y) {
  mp_real r(maxprec(x, y));
  mpfr_add(r.m, x.m, y.m, MPFR_RNDN);
  return r;
}

inline mp_real operator-(mp_real const &x, mp_real const &y) {
  mp_real r(maxprec(x, y));
  mpfr_sub(r.m, x.m, y.m, MPFR_RNDN);
  return r;
}

inline mp_real operator*(mp_real const &x, mp_real const &y) {
  mp_real r(maxprec(x, y));
  mpfr_mul(r.m, x.m, y.m, MPFR_RNDN);
  return r;
}

inline mp_real operator/(mp_real const &x, mp_real const &y) {
  mp_real r(maxprec(x, y));
  mpfr_div(r.m, x.m, y.m, MPFR_RNDN);
  return r;
}

inline mp_real sqr(mp_real const &x) {
  mp_real r(mpfr_get_prec(x.m));
  mpfr_sqr(r.m, x.m, MPFR_RNDN);
  return r;
}

inline mp_real sqrt(mp_real const &x) {
  mp_real r(mpfr_get_prec(x.m));
  mpfr_sqrt(r.m, x.m, MPFR_RNDN);
  return r;
}

inline mp_real log(mp_real const &x) {
  mp_real r(mpfr_get_prec(x.m));
  mpfr_log(r.m, x.m, MPFR_RNDN);
  return r;
}

inline mp_real atan2(mp_real const &y, mp_real const &x) {
  mp_real r(maxprec(x, y));
  mpfr_atan2(r.m, y.m, x.m, MPFR_RNDN);
  return r;
}

inline mp_real pow(mp_real const &y, mp_real const &x) {
  mp_real r(maxprec(x, y));
  mpfr_pow(r.m, y.m, x.m, MPFR_RNDN);
  return r;
}

inline float to_float(mp_real const &x) {
  return mpfr_get_d(x.m, MPFR_RNDN);
}

#undef maxprec

#endif
