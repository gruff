uniform sampler2D red;
uniform sampler2D green;
uniform sampler2D blue;

void main(void) {
  float r = texture2D(red,   gl_TexCoord[0].st).x;
  float g = texture2D(green, gl_TexCoord[0].st).x;
  float b = texture2D(blue,  gl_TexCoord[0].st).x;
  gl_FragColor = vec4(r, g, b, 1.0);
}
