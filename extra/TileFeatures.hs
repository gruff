{-# LANGUAGE BangPatterns #-}
import Control.Exception (evaluate)
import Control.Monad (forM)
import Control.Monad.Instances ()
import Data.List (nubBy)
import Data.Maybe (mapMaybe,catMaybes)
import Numeric.QD.Vec ()

import Foreign (Ptr, peekElemOff)
import Foreign.C (CFloat)

import Fractal.RUFF.Mandelbrot.Atom
import Fractal.RUFF.Mandelbrot.Address
import Fractal.RUFF.Types.Complex

computeFeatures :: Tile -> IO [(MuAtom Double, AngledInternalAddress)]
computeFeatures (Tile q _ ds _) = do
  let s = square rootSquare q
      cx, cy, sx, sy :: Double
      cx = fromRational (squareWest s)
      cy = fromRational (squareNorth s)
      cs = fromRational (squareSize s)
      sx = cs / fromIntegral width
      sy = cs / fromIntegral height
      inq (x:+y) = cx <= x && x <= cx + cs && cy <= y && y <= cy + cs
  sss <- forM [2,4,8] $ \d -> do
    let dx = width  `div` d
        dy = height `div` d
        c i j = (cx + fromIntegral (i * dx) * sx) :+ (cy + fromIntegral (j * dy) * sy)
        r = 2 * cs / fromIntegral d
    forM [1 .. d - 1] $ \j -> do
      forM [1 .. d - 1] $ \i -> do
        b <- containsBorder ds (j * dy) (i * dx) dy dx
        return $ if b then locate_ (c i j) r else Nothing
  let fs = mapMaybe (\ !(mu@MuAtom{ muNucleus = mx :+ my }) -> fmap ((,) mu) (findAddress' (take 10000 $ findAddress (mu{ muNucleus = realToFrac mx :+ realToFrac my :: Complex DoubleDouble })))) . nubBy muEqual . filter (inq . muNucleus) . catMaybes . concat . concat $ sss
  !hack <- evaluate (length (show fs)) -- force evaluation in this thread
  return fs

containsBorder :: Ptr CFloat -> Int -> Int -> Int -> Int -> IO Bool
containsBorder p y x dy dx = do
  any (\d -> 0 < d && d < 4) `fmap` sequence [ peekElemOff p (width * j + i) | j <- [ y - dy .. y + dy - 1], i <- [x - dx .. x + dx - 1] ]

muEqual :: MuAtom Double -> MuAtom Double -> Bool
muEqual m n = magnitude2 (muNucleus m - muNucleus n) < (muSize m * muSize m + muSize n * muSize n) / 4


--    , windowDecorated := False
module Overlay where

import Graphics.Rendering.OpenGL

import Number (R)

overlay :: Size -> TextureObject -> IO ()
overlay (Size w h) t = do

type Image = StorableArray (Int, Int, Int) Word8
type Bitmap = StorableArray (Int, Int) Bool

getPixels :: StorableArray (Int, Int, Int) Word8 -> IO ()
getPixels a = do
  ((y0,x0,c0),(y1,x1,c1)) <- getBounds a
  let w = x1 - x0 + 1
      h = y1 - y0 + 1
  guard  (c1 - c0 + 1 == 3)
  withStorableArray a $ \ptr ->
    readPixels (Position (fromIntegral x0) (fromIntegral y0)) (Size (fromIntegral w) (fromIntegral h)) (PixelData RGB UnsignedByte ptr)

getAtoms :: Bitmap -> IO [MuAtom]
getAtoms a = do
  bs@((y0,x0),(y1,x1)) <- getBounds a
  
