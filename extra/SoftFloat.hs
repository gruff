{-# LANGUAGE BangPatterns, DeriveDataTypeable #-}

module SoftFloat where

import Control.Monad.Instances ()
import Data.Bits (bit, shiftL, shiftR)
import Data.Int (Int32, Int64)
import Data.Monoid (mappend)
import Data.Ratio ((%), numerator, denominator)
import Numeric (readSigned, readFloat, showSigned, showGFloat)
import Data.Data (Data)
import Data.Typeable(Typeable)

type Mantissa = Int32
type Mantissa' = Int64
type Exponent = Int32

data SoftFloat = SoftFloat !Mantissa !Exponent
  deriving (Data, Typeable)

up :: Mantissa -> Mantissa'
up = fromIntegral

down :: Mantissa' -> Mantissa
down = fromIntegral

softFloat :: Mantissa' -> Exponent -> SoftFloat
softFloat 0 !_ = SoftFloat 0 0
softFloat m !e
  | m < up minBound || up maxBound < m = softFloat (m `shiftR` 1) (e + 1)
  | up minBound `shiftR` 1 < m && m < up maxBound `shiftR` 1 = softFloat (m `shiftL` 1) (e - 1)
  | otherwise = SoftFloat (down m) e

up' :: Mantissa -> Integer
up' = fromIntegral

down' :: Integer -> Mantissa
down' = fromIntegral

softFloat' :: Integer -> Mantissa -> SoftFloat
softFloat' 0 !_ = SoftFloat 0 0
softFloat' m !e
  | m < up' minBound || up' maxBound < m = softFloat' (m `shiftR` 1) (e + 1)
  | up' minBound `shiftR` 1 < m && m < up' maxBound `shiftR` 1 = softFloat' (m `shiftL` 1) (e - 1)
  | otherwise = SoftFloat (down' m) e

instance Show SoftFloat where
  show f = showSigned (showGFloat (Just 12)) 9 f ""

instance Read SoftFloat where
  readsPrec _ = readSigned readFloat

instance Eq SoftFloat where
  SoftFloat m e == SoftFloat m' e' = m == m' && e == e'
  SoftFloat m e /= SoftFloat m' e' = m /= m' || e /= e'

instance Ord SoftFloat where
  SoftFloat m e `compare` SoftFloat m' e'
    | m  == 0 = 0 `compare` m'
    | m' == 0 = m `compare` 0
    | m > 0 && m' > 0 = (e `compare` e') `mappend` (m `compare` m')
    | m > 0 && m' < 0 = GT
    | m < 0 && m' > 0 = LT
    | m < 0 && m' < 0 = (e' `compare` e) `mappend` (m `compare` m')
    | otherwise = error "gruff.SoftFloat.Ord.compare"

instance Num SoftFloat where
  SoftFloat 0 _ + f = f
  f + SoftFloat 0 _ = f
  SoftFloat m e + SoftFloat m' e'
    | e >  e' = softFloat (up m + (up m' `shiftR` fromIntegral (e - e'))) e
    | e == e' = softFloat (up m + up m') e
    | e <  e' = softFloat ((up m `shiftR` fromIntegral (e' - e)) + up m') e'
    | otherwise = error "gruff.SoftFloat.Num.+"
  SoftFloat 0 _ - f = negate f
  f - SoftFloat 0 _ = f
  SoftFloat m e - SoftFloat m' e'
    | e >  e' = softFloat (up m - (up m' `shiftR` fromIntegral (e - e'))) e
    | e == e' = softFloat (up m - up m') e
    | e <  e' = softFloat ((up m `shiftR` fromIntegral (e' - e)) - up m') e'
    | otherwise = error "gruff.SoftFloat.Num.-"
  negate (SoftFloat m e) = SoftFloat (negate m) e
  f@(SoftFloat 0 _) * _ = f
  _ * f@(SoftFloat 0 _) = f
  SoftFloat m e * SoftFloat m' e' = softFloat (up m * up m') (e + e')
  fromInteger i = softFloat' i 0 -- FIXME large integers
  abs (SoftFloat m e) = SoftFloat (abs m) e
  signum (SoftFloat m _) = softFloat (up $ signum m) 0

instance Fractional SoftFloat where
  f@(SoftFloat 0 _) / _ = f
  _ / (SoftFloat 0 _) = error "/0"
  SoftFloat m e / SoftFloat m' e' = softFloat ((up m `shiftL` 32) `div` up m') (e - e' - 32)
  fromRational r = fromInteger (numerator r) / fromInteger (denominator r)

instance Real SoftFloat where
  toRational (SoftFloat m e)
    | e >= 0 = fromInteger (toInteger m * bit (fromIntegral e))
    | e <  0 = toInteger m % bit (negate (fromIntegral e))
    | otherwise = error "gruff.SoftFloat.Real.toRational"

instance RealFrac SoftFloat where
  properFraction f@(SoftFloat m e)
    | e >= 0 = (fromIntegral m * fromInteger (bit (fromIntegral e)), 0)
    | e <= (-31) = (0, f)
    | otherwise = (fmap fromRational . properFraction . toRational) f

instance Floating SoftFloat where
  pi = realToFrac (pi :: Double)
  sqrt f = go 1
    where
      go !r =
        let r' = (r + f / r) / 2
        in  if r == r' then r else go r'
  exp f = go 0 1 1 1
    where
      go !e !nf !fn !n =
        let e' = e + fn / nf
        in  if e == e' then e else go e' (nf * n) (f * fn) (n + 1)
  log f@(SoftFloat _ e)
    | f > 0 = pi / (2 * agm 1 (2^^(2 - m) / f)) - fromIntegral m * ln2
    | otherwise = error "gruff.SoftFloat.Floating.log"
    where
      m = 37 - e
      agm !a! b =
        let a' = (a + b) / 2
            b' = sqrt (a * b)
        in  if a' == b' || (a == a' && b == b') then a' else agm a' b'
      ln2 = realToFrac (log 2 :: Double)

  sin = viaDouble sin
  cos = viaDouble cos
  tan = viaDouble tan
  sinh = viaDouble sinh
  cosh = viaDouble cosh
  tanh = viaDouble tanh
  asin = viaDouble asin
  acos = viaDouble acos
  atan = viaDouble atan
  asinh = viaDouble asinh
  acosh = viaDouble acosh
  atanh = viaDouble atanh

viaDouble :: (Double -> Double) -> (SoftFloat -> SoftFloat)
viaDouble f = uncurry encodeFloat . decodeFloat . checkFloat . f . uncurry encodeFloat . decodeFloat

checkFloat :: Double -> Double
checkFloat f
  | isNaN f = error "NaN"
  | isInfinite f = error "Inf"
  | otherwise = f

instance RealFloat SoftFloat where
  floatRadix _ = 2
  floatDigits _ = 31
  floatRange _ = (fromIntegral (minBound :: Exponent), fromIntegral (maxBound :: Exponent))
  decodeFloat (SoftFloat m e) = (fromIntegral m, fromIntegral e)
  encodeFloat m e = softFloat' m (fromIntegral e)
  isNaN _ = False
  isInfinite _ = False
  isDenormalized _ = False
  isNegativeZero _ = False
  isIEEE _ = False
