{-# LANGUAGE BangPatterns, EmptyDataDecls, Rank2Types #-}
module FixedPrecision where

import Data.Bits (bit, shiftL, shiftR)
import Data.Ratio ((%), numerator, denominator)
import TypeLevel.NaturalNumber -- type-level-natural-number
import Text.FShow.Raw -- floatshow

newtype Fixed p = F Integer

getPrecision :: Fixed p -> p
getPrecision _ = undefined

withPrecision :: NaturalNumber p => Int -> p -> (forall q. NaturalNumber q => q -> a) -> a
withPrecision 0 p f = f p
withPrecision n p f = withPrecision (n - 1) (successorTo p) f

instance NaturalNumber p => BinDecode (Fixed p) where
  decode l@(F x) = (x, negate . naturalNumberAsInt . getPrecision $ l)
  showDigits l = 2 + floor (fromIntegral (1 + naturalNumberAsInt (getPrecision l)) * logBase 10 2 :: Double)

showF :: NaturalNumber p => Fixed p -> String
showF x = binDecFormat (Generic (Just (-5, 5))) Nothing x

instance Eq (Fixed p) where F x == F y = x == y
instance Ord (Fixed p) where F x `compare` F y = x `compare` y
instance NaturalNumber p => Num (Fixed p) where
  F x + F y = F $  x + y
  F x - F y = F $  x - y
  l@(F x) * F y = F $ (x * y) `shiftR` naturalNumberAsInt (getPrecision l)
  negate (F x) = F (negate x)
  abs (F x) = F (abs x)
  signum (F x) = F (signum x)
  fromInteger x = let r = F (x `shiftL` naturalNumberAsInt (getPrecision r)) in r
instance NaturalNumber p => Fractional (Fixed p) where
  l@(F x) / F y = F ((x `shiftL` naturalNumberAsInt (getPrecision l)) `div` y)
  fromRational x =
    let r = F ((numerator x `shiftL` naturalNumberAsInt (getPrecision r)) `div` denominator x) in r
instance NaturalNumber p => Real (Fixed p) where
  toRational l@(F x) = x % bit (naturalNumberAsInt $ getPrecision l)

incrPrecision :: NaturalNumber p => Fixed p -> Fixed (SuccessorTo p)
incrPrecision (F x) = F (x `shiftL` 1)

decrPrecision :: NaturalNumber p => Fixed (SuccessorTo p) -> Fixed p
decrPrecision (F x) = F (x `shiftR` 1)
