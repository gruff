#include <stdio.h>
#include <stdlib.h>

#define TSIZE 256

int main(int argc, char **argv) {
  char header[16];
  const unsigned int count = TSIZE * TSIZE;
  const unsigned int bytes = count * sizeof(float);
  float *dwell = malloc(bytes);
  if (! dwell) { return 1; }
  if (1 != fread(header, 16, 1, stdin)) { free(dwell); return 1; }
  if (1 != fread(dwell, bytes, 1, stdin)) { free(dwell); return 1; }
  double score = 0;
  for (unsigned int i = 0; i < count; ++i) {
    double d = dwell[i];
    if (d > 0) { score += d; }
  }
  printf("%.16e\n", score);
  free(dwell);
  return 0;
}
