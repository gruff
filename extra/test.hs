import Data.List (nub)
import Data.Maybe (catMaybes, mapMaybe)
import Data.Ratio ((%))
import System.Environment (getArgs)
import System.IO (hFlush, stdout)

import Fractal.RUFF.Mandelbrot.Address (AngledInternalAddress, splitAddress, parseAngledInternalAddress, prettyAngledInternalAddress, angledInternalAddress)
import Fractal.RUFF.Types.Complex (imagPart)

import MuAtom

{-
ray tracing failures (wanted from angle : found from reverse trace)
  1 1/3 3 6 7 : 1 1/3 3 4 5 7
  1 1/3 3 4 7 : 1 1/3 3 4 5 7
  1 2 1/3 6 7 : 1 2 1/3 5 6 7
  1 2 1/3 5 7 : 1 2 1/3 5 6 7
  1 1/3 3 6 8 : 1 1/3 3 5 6 8
  1 1/3 3 5 8 : 1 1/3 3 5 6 8
  1 1/3 3 4 7 8 : 1 1/3 3 4 5 6 8
  1 1/3 3 4 5 8 : 1 1/3 3 4 5 6 8
  1 2 1/3 6 8 : 1 2 1/3 5 6 7 8
  1 2 1/3 5 7 8 : 1 2 1/3 5 6 7 8
  1 2 1/3 5 6 8 : 1 2 1/3 5 6 7 8
  1 2 3 1/3 8 : 1 2 3 1/3 7 8
ie, 12 unexpected failures from 116 test cases
FIXED: silly assumption in forward trace finding ray end...

-- reverse ray tracing failures:
islands' :: [AngledInternalAddress]
islands' = mapMaybe parseAngledInternalAddress
  [ "1 2 1/3 5 6 8 9"
  , "1 1/6 6 8 10"
  , "1 1/5 5 8 9 10"
  , "1 1/3 3 6 2/3 10"
  , "1 1/3 3 2/3 8 9 10"
  , "1 3/8 8 9 10"
  , "1 2/5 5 8 10"
  , "1 2 1/3 6 7 8 10"
  ]
-- ie, 8 unexpected failures from 510 test cases
-- FIXED: not enough accuracy (1e-8 -> 1e-16) or sharpness (8 -> 16) in ray trace...

-- current regressions FAIL (reverse trace):
islands' :: [AngledInternalAddress]
islands' = mapMaybe parseAngledInternalAddress
  [ "1 1/4 4 6 8"
  , "1 3/8 8 9"
  , "1 2 3 5 1/3 9"
  , "1 1/6 6 7 10"
  , "1 1/3 3 4 6 7 8 10"
  , "1 2/5 5 6 8 10"
  , "1 1/7 7 9 11"
  ]

-}

main :: IO ()
main = do
  args <- getArgs
  let n = case map reads args of
              [[(i, "")]] -> i
              _ -> 0
  mapM_ test (drop n islands)

islands :: [AngledInternalAddress]
islands = (nub . filter island . catMaybes . map angledInternalAddress . nub . filter (0.5 >)) [ num % den | p <- [ 2 .. ], let den = 2 ^ (p :: Int) - 1, num <- [ 1 .. den - 1] ]
  where island = null . snd . splitAddress

test :: AngledInternalAddress -> IO ()
test addr = putStr (prettyAngledInternalAddress addr ++ " : ") >> hFlush stdout >> case last (take limit $ muFromAddress addr) of
  MuSuccess mu
    | (1e-16 <) . abs . imagPart . muNucleus $ mu ->
        case last (take limit $ muToAddress mu) of
          MuSuccess' addr'
            | addr == addr' -> putStrLn "OK"
            | otherwise -> putStrLn (prettyAngledInternalAddress addr' ++ " FAIL")
          e -> putStrLn $ "FAIL (reverse trace) " ++ show e
    | otherwise -> putStrLn "AXIS"
  e -> putStrLn $ "FAIL (forward trace) " ++ show e

limit :: Int
limit = 10000
