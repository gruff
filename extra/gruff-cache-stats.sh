#!/bin/bash
cachedir="${HOME}/.gruff/cache/"
find "${cachedir}" -type f |
while read fname
do
  score="$(./gruff-tile-score < "${fname}")"
  echo "${score} ${fname}"
done
