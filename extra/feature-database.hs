{-# LANGUAGE GeneralizedNewtypeDeriving #-}
import Data.List (intercalate)
import Data.Ratio ((%), numerator, denominator)

import qualified Data.Map as M

import Data.Vec (NearZero(nearZero))
import Data.Number.MPFR (MPFR, RoundMode(Near, Up), Precision, getPrec, int2w, fromIntegerA, toString)
import Data.Number.MPFR.Instances.Near ()

import Fractal.RUFF.Types.Complex (realPart, imagPart)
import Fractal.RUFF.Mandelbrot.Address
  ( Angle, period, kneading
  , AngledInternalAddress(..), angledInternalAddress
  , addressPeriod, prettyAngledInternalAddress
  )
import Fractal.RUFF.Mandelbrot.Atom (MuAtom(..), findAtom_)

instance NearZero MPFR where
  nearZero x = let p = getPrec x in not (abs x > int2w Up p 1 (4 - fromIntegral p))

newtype R = R{ unR :: MPFR }
  deriving (Eq, Ord, Floating, Real, RealFrac, NearZero)

instance Num R where
  R a + R b = R (a + b)
  R a * R b = R (a * b)
  R a - R b = R (a - b)
  negate (R a) = R (negate a)
  abs (R a) = R (abs a)
  signum (R a) = R (signum a)
  fromInteger i = R (fromIntegerA Near bits i)

instance Fractional R where
  R a / R b = R (a / b)
  recip (R a) = R (recip a)
  fromRational r = R (fromIntegerA Near bits (numerator r) / fromIntegerA Near bits (denominator r))

instance Show R where
  show (R m) = toString (ceiling $ (2::Double) + log 2 / log 10 * fromIntegral (getPrec m)) m

bits :: Precision
bits = 1000

isIsland :: AngledInternalAddress -> Bool
isIsland (Unangled 1) = True
isIsland (Angled p r (Unangled q)) = q /= p * denominator r
isIsland (Angled _ _ a) = isIsland a

angles :: [(AngledInternalAddress, (Angle, Angle))]
angles = concat $
  [ merge M.empty $
    [ (addr, a)
    | n <- [ 0 .. d ]
    , let a = n % d
    , (period . kneading) a == Just p
    , let Just addr = angledInternalAddress a
    ]
  | p <- [ 1 .. 16 ]
  , let d = 2 ^ p - 1
  ]

merge :: M.Map AngledInternalAddress Angle -> [(AngledInternalAddress, Angle)] -> [(AngledInternalAddress, (Angle, Angle))]
merge m [] | M.null m = []
merge m ((addr, a):rest) = case M.lookup addr m of
  Nothing -> merge (M.insert addr a m) rest
  Just b -> (addr, (b, a)) : merge (M.delete addr m) rest

pretty :: (AngledInternalAddress, (Angle, Angle)) -> String
pretty (addr, (lo, hi)) = intercalate "," $
  [ show (addressPeriod addr)
  , show (isIsland addr)
  , prettyAngledInternalAddress addr
  , show (numerator   lo)
  , show (denominator lo)
  , show (numerator   hi)
  , show (denominator hi)
  ] ++ case (isIsland addr && addressPeriod addr > 1, findAtom_ addr) of
    (False, _) -> ["","","",""]
    (True, Just mu) ->
      let digits = ceiling $ 8 - logBase 10 (muSize mu)
          showR = toString digits . unR
      in  [ show  . muOrient $ mu
          , show  . muSize   $ mu
          , showR . realPart . muNucleus $ mu
          , showR . imagPart . muNucleus $ mu
          ]
    _ -> ["","","",""]

main :: IO ()
main = mapM_ (putStrLn . pretty) angles
