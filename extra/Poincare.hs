import Data.Maybe (mapMaybe)
import Numeric.LinearAlgebra ((><), nullVector, toList)
import Numeric.QD (QuadDouble)
import Numeric.QD.Vec ()
import Fractal.GRUFF
import Fractal.RUFF.Mandelbrot.Address (parseAngledInternalAddress)
import Fractal.RUFF.Mandelbrot.Atom (MuAtom(..), findAtom_)
import Fractal.RUFF.Types.Complex(Complex((:+)), magnitude, magnitude2, cis, phase)

import Debug.Trace

debug p x = trace (p ++ ": " ++ show x) x

type R = QuadDouble

data Poincare a = Poincare !(Complex a) !a
  deriving Show

distance (Poincare x1 y1) (Poincare x2 y2) = acosh (1 + (magnitude2 dx + dy * dy) / (2 * y1 * y2))
  where
    dx = x2 - x1
    dy = y2 - y1

geodesic p1@(Poincare x1 y1) p2@(Poincare x2 y2)
  | x1 == x2  = \t -> Poincare x1 (y1 * (y2 / y1) ** t)
  | otherwise = \t -> let x:+y = go (exp (t * dt)) in Poincare (x1 + (x :+ 0) * dir) y
  where
    a =  d * y1
    b = -c * y1
    [c, d] = debug "cd" . normalize (map realToFrac . (id :: [Double] -> [Double]) . toList . nullVector . (2><2) . map realToFrac $ ms
    dt = distance p1 p2
    eT = exp dt
    ms = debug "ms" [ eT * mdx, y2 - y1 * eT, y1 - eT * y2, mdx ]
    mdx = magnitude dx
    dir = cis (phase dx)
    dx = x2 - x1
    go t = (b :+ (a * t)) / (d :+ (c * t))

{-
    (a i     + b) / (c i     + d) =    0    :+ y1
    (a i e^T + b) / (c i e^T + d) = |x2-x1| :+ y2
=>    
    (a i     + b) = (c i     + d) * (   0    :+ y1)
    (a i e^T + b) = (c i e^T + d) * (|x2-x1| :+ y2)
=>
    a   =  d * y1
    b   = -c * y1
    a e = c e |x2 - x1| + d y2
    b   = d   |x2 - x1| - c e y2
=>
    d y1 e = c e |x2 - x1| + d y2
    -c y1  = d   |x2 - x1| - c e y2
=>
    c e |x2 - x1|    + d (y2 - y1 e) = 0
    c (-(e y2 - y1)) + d |x2 - x1|   = 0
-}

poincare mu = Poincare (muNucleus mu) (16 * realToFrac (muSize mu))

mus = mapMaybe (\s -> findAtom_ =<< parseAngledInternalAddress s)
        [ "1 1/3 3 " ++ (unwords . map show . take m) [(4 :: Int) .. ] | m <- [1 .. 16] ]

pcs :: [Poincare R]
pcs = Poincare 0 4 : map poincare mus ++ [ Poincare 0 4 ]

dgs = zipWith (\p q -> (distance p q, geodesic p q)) pcs (tail pcs)

fps = 25

segment (d, g) = segment' 0
  where
    segment' n | n > fps * d = []
               | otherwise = g (n / (fps * d)) : segment' (n + 1)

scenes = zipWith scene [0..] (concatMap segment dgs)

scene s (Poincare (cx :+ cy) r) =
  let f = filename s
      i = Image
            { imageLocation = Location
                { center = toRational cx :+ toRational cy
                , radius = realToFrac r
                }
            , imageViewport = Viewport
                { aspect = 1080 / 576
                , orient = 0
                }
            , imageWindow   = Window
                { width = 1080
                , height = 576
                , supersamples = 4
                }
            , imageColours  = Colours
                { colourInterior = Colour 1 0.5 0
                , colourBoundary = Colour 0 0 0
                , colourExterior = Colour 1 1 1
                }
            , imageLabels   = []
            , imageLines    = []
            }
  in (i, f)

filename :: Integer -> FilePath
filename s = (reverse . take 8 . (++ "00000000") . reverse . show) s ++ ".ppm"

main = defaultMain scenes
