module Main (main) where

import System.FilePath ((</>))
import System.Directory (getAppUserDataDirectory)
import System.IO
import Control.Monad (forM_, when)
import Data.IORef
import Foreign (malloc, nullPtr, allocaBytes, peek, with, sizeOf, alloca, Ptr)
import Foreign.C (CFloat)
import Numeric (readSigned, readFloat)
import QuadTree
import Shader
import Tile (Tile(..), getTile, freeTile, rootSquare)
import Graphics.Rendering.OpenGL hiding (Position, Size)
import qualified Graphics.Rendering.OpenGL as GL
import Graphics.Rendering.OpenGL.Raw
  ( glGenFramebuffers, glBindFramebuffer, glFramebufferTexture2D, glGenerateMipmap
  , gl_FRAMEBUFFER, gl_COLOR_ATTACHMENT0, gl_TEXTURE_2D
  , glTexImage2D, gl_R32F, gl_RGBA32F, gl_LUMINANCE, gl_FLOAT, gl_RGBA, gl_UNSIGNED_BYTE
  , gl_FALSE, glClampColor, gl_CLAMP_VERTEX_COLOR, gl_CLAMP_READ_COLOR
  , gl_CLAMP_FRAGMENT_COLOR )
import qualified Graphics.UI.GLUT as GLUT

data E = E{ outer, inner, result :: TextureObject, qss :: [[Quad]], firstPass :: Bool, combine, blender :: Program, fbo :: GLuint, cache :: String
          , center :: (Rational, Rational), level :: Int, outh :: Handle }

texturef s = do
  [tex] <- genObjectNames 1
  texture Texture2D $= Enabled
  textureBinding Texture2D $= Just tex
  glTexImage2D gl_TEXTURE_2D 0 (fromIntegral gl_RGBA) s s 0 gl_RGBA gl_UNSIGNED_BYTE nullPtr
  textureFilter Texture2D $= ((Linear', Just Linear'), Linear')
  textureWrapMode Texture2D S $= (Repeated, ClampToEdge)
  textureWrapMode Texture2D T $= (Repeated, ClampToEdge)
  textureBinding Texture2D $= Nothing
  texture Texture2D $= Disabled
  return tex  

main :: IO ()
main = do
  GLUT.initialWindowSize $= GL.Size 400 44
  GLUT.initialDisplayMode $= [GLUT.RGBAMode, GLUT.DoubleBuffered]
  (_, [sre, sim]) <- GLUT.getArgsAndInitialize
  _ <- GLUT.createWindow "expo"
  glClampColor gl_CLAMP_VERTEX_COLOR gl_FALSE
  glClampColor gl_CLAMP_READ_COLOR gl_FALSE
  glClampColor gl_CLAMP_FRAGMENT_COLOR gl_FALSE
  drawBuffer $= BackBuffers
  fbo' <- alloca $ \p -> glGenFramebuffers 1 p >> peek p
  outer'  <- texturef 256
  inner'  <- texturef 256
  result' <- texturef 512
  combine' <- shader Nothing (Just "colourize.frag")
  blender' <- shader Nothing (Just "expblend.frag")
  appDir <- getAppUserDataDirectory "gruff"
  let cacheDir = appDir </> "cache"
      rootRegion x y = Region{ regionWest = x - 2, regionEast = x + 2, regionNorth = y - 2, regionSouth = y + 2 }
      [(cx0, "")] = readSigned readFloat sre
      [(cy0, "")] = readSigned readFloat sim
      center0 = (cx0, cy0)
      target = rootRegion cx0 cy0
      eqs = equads rootSquare target
  outh' <- openBinaryFile "out.raw" WriteMode
  eR <- newIORef E{ outer = outer', inner = inner', result = result', qss = tail eqs, firstPass = True, combine = combine', blender = blender', fbo = fbo', center = center0, level = 0, cache = cacheDir, outh = outh' }
  GLUT.displayCallback $= eRender eR
  GLUT.mainLoop

eRender eR = do
  e <- readIORef eR
  let qs = head (qss e)
  viewport $= (GL.Position 0 0, GL.Size 256 256)
  loadIdentity
  ortho2D 0 256 256 0
  withFBO (fbo e) (inner e) $ do
   forM_ qs $ \q -> do
    Just t@(Tile _ ns ds ts) <- with 0 $ \p -> getTile putStrLn (cache e) p q
    tit <- upload ns
    tde <- upload ds
    ttt <- upload ts
    freeTile t
    currentProgram $= Just (combine e)
    lit <- get $ uniformLocation (combine e) "it"
    lde <- get $ uniformLocation (combine e) "de"
    ltt <- get $ uniformLocation (combine e) "tt"
    lsh <- get $ uniformLocation (combine e) "hshift"
    lsc <- get $ uniformLocation (combine e) "hscale"
    uniform lit $= TexCoord1 (0 :: GLint)
    uniform lde $= TexCoord1 (1 :: GLint)
    uniform ltt $= TexCoord1 (2 :: GLint)
    uniform lsh $= TexCoord1 (0 :: GLfloat)
    uniform lsc $= TexCoord1 (0.5 :: GLfloat)
    activeTexture $= TextureUnit 0
    textureBinding Texture2D $= Just tit
    activeTexture $= TextureUnit 1
    textureBinding Texture2D $= Just tde
    activeTexture $= TextureUnit 2
    textureBinding Texture2D $= Just ttt
    renderPrimitive Quads $ do
      let sq = square rootSquare q
          t x y = texCoord $ TexCoord2 (x :: GLdouble) (y :: GLdouble)
          v x y = let (x', y') = toPixel e x y
                  in  vertex $ Vertex2
                        (fromRational x' :: GLdouble)
                        (fromRational y' :: GLdouble)
          x0 = squareWest sq
          x1 = squareWest sq  + squareSize sq
          y0 = squareNorth sq
          y1 = squareNorth sq + squareSize sq
      color $ Color3 1 1 (1::GLdouble)
      t 0 1 >> v x0 y1
      t 0 0 >> v x0 y0
      t 1 0 >> v x1 y0
      t 1 1 >> v x1 y1
    textureBinding Texture2D $= Nothing
    activeTexture $= TextureUnit 1
    textureBinding Texture2D $= Nothing
    activeTexture $= TextureUnit 0
    textureBinding Texture2D $= Nothing
    currentProgram $= Nothing
    deleteObjectNames [tit, tde, ttt]
  textureBinding Texture2D $= Just (inner e)
  glGenerateMipmap gl_TEXTURE_2D
  textureBinding Texture2D $= Nothing
  when (not (firstPass e)) $ do
    viewport $= (GL.Position 0 0, GL.Size 400 44)
    loadIdentity
    ortho2D 0 1 0 1
    do
      currentProgram $= Just (blender e)
      lo <- get $ uniformLocation (blender e) "outer"
      li <- get $ uniformLocation (blender e) "inner"
      uniform lo $= TexCoord1 (0 :: GLint)
      uniform li $= TexCoord1 (1 :: GLint)
      activeTexture $= TextureUnit 0
      textureBinding Texture2D $= Just (outer e)
      activeTexture $= TextureUnit 1
      textureBinding Texture2D $= Just (inner e)
      let t x y = texCoord $ TexCoord2 (x :: GLdouble) (y :: GLdouble)
          v x y = vertex $ Vertex2 (x :: GLdouble) (y :: GLdouble)
      renderPrimitive Quads $ do
        color $ Color3 1 1 (1::GLdouble)
        t 0        1 >> v 0 1
        t 0        0 >> v 0 0
        t (2 * pi) 0 >> v 1 0
        t (2 * pi) 1 >> v 1 1
      textureBinding Texture2D $= Nothing
      activeTexture $= TextureUnit 0
      textureBinding Texture2D $= Nothing
      currentProgram $= Nothing
      let bytes = 400 * 44 * 3
      allocaBytes bytes $ \ptr -> do
        readPixels (GL.Position 0 0) (GL.Size 400 44) $ PixelData RGB UnsignedByte ptr
        hPutBuf (outh e) ptr bytes
        hFlush (outh e)
  atomicModifyIORef eR $ \e' -> (e' { outer = inner e', inner = outer e', qss = tail (qss e'), firstPass = False, level = level e + 1 }, ())
  GLUT.swapBuffers
  GLUT.reportErrors
  GLUT.postRedisplay Nothing

withFBO :: GLuint -> TextureObject -> IO a -> IO a
withFBO fbo (TextureObject tex) act = do
  glBindFramebuffer gl_FRAMEBUFFER fbo
  glFramebufferTexture2D gl_FRAMEBUFFER gl_COLOR_ATTACHMENT0 gl_TEXTURE_2D tex 0
  r <- act
  glFramebufferTexture2D gl_FRAMEBUFFER gl_COLOR_ATTACHMENT0 gl_TEXTURE_2D 0 0
  glBindFramebuffer gl_FRAMEBUFFER 0
  return r

toPixel :: E -> Rational -> Rational -> (Rational, Rational)
toPixel e = f
  where
    f x y = (x', y')
      where
        x' = ((x - cx) / r' + 0.5) * w'
        y' = ((y - cy) / r  + 0.5) * h'
    w' = 256
    h' = 256
    a = w' / h'
    (cx, cy) = center e
    r = 8 / 2 ^ level e * h' / (2 * 256)
    r' = a * r

upload :: Ptr CFloat -> IO TextureObject
upload p = do
  [tex] <- genObjectNames 1
  texture Texture2D $= Enabled
  textureBinding Texture2D $= Just tex
  glTexImage2D gl_TEXTURE_2D 0 (fromIntegral gl_R32F) 256 256 0 gl_LUMINANCE gl_FLOAT p
  textureFilter Texture2D $= ((Nearest, Nothing), Nearest)
  textureWrapMode Texture2D S $= (Repeated, ClampToEdge)
  textureWrapMode Texture2D T $= (Repeated, ClampToEdge)
  textureBinding Texture2D $= Nothing
  texture Texture2D $= Disabled
  return tex
