{-# LANGUAGE CPP, GeneralizedNewtypeDeriving #-}
module Number (R, toRational') where

import Data.Bits (bit)
import Data.Ratio ((%))
import Data.Vec (NearZero)

import Numeric.QD (QuadDouble)
import Numeric.QD.Vec ()

type R = QuadDouble

toRational' :: RealFrac a => Int -> a -> Rational
toRational' l a = round (a * fromInteger b) % b
  where b = bit (l + 16)
